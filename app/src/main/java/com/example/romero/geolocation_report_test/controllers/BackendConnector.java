package com.example.romero.geolocation_report_test.controllers;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.example.romero.geolocation_report_test.models.Report;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by romero on 08/07/15.
 */
public class BackendConnector {

    private static final String HOST = "http://192.168.1.101:5000/";
    private static final String API_KEY = "dt5nF0xBPNQ61dqQEofuFO5t4x9iu8l6";

    public void sendNewReport(Report report, Context context) throws InterruptedException, ExecutionException, TimeoutException, JSONException {
        String url = HOST + "report";

        // Body params to be sent to the server
        HashMap<String, Object> body_params = new HashMap<String, Object>();
        body_params.put("device_id", report.getDeviceId());
        body_params.put("type", report.getType().name());
        body_params.put("longitude", report.getLongitude());
        body_params.put("latitude", report.getLatitude());

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(body_params), future, future){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("API-Key", API_KEY);
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(req);

        JSONObject response = future.get(30, TimeUnit.SECONDS);

        VolleyLog.v("Response:%n %s", response.toString());
        if (response.has("id")) {
            report.setId(response.getString("id"));
        }
    }

    public void editReport(Report report, Context context) throws InterruptedException, ExecutionException, TimeoutException, JSONException {
        String reportDescription = report.getDescription();
        String reportId = report.getId();
        if (    reportDescription == null || reportDescription.isEmpty()
                || reportId == null || reportId.isEmpty()) {
            throw new IllegalArgumentException();
        }

        String url = HOST + "report/" + reportId;

        // Body params to be sent to the server
        HashMap<String, Object> body_params = new HashMap<String, Object>();
        body_params.put("description", reportDescription);

        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(body_params), future, future){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("API-Key", API_KEY);
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(req);

        JSONObject response = future.get(30, TimeUnit.SECONDS);

        VolleyLog.v("Response:%n %s", response.toString());
        if (response.has("description")) {
            report.setId(response.getString("description"));
        }
    }

    public void sendReportVoiceNote(Report report) throws MalformedURLException, FileNotFoundException, IOException {
        File voiceNote = report.getVoiceNote();
        String reportId = report.getId();
        if (voiceNote == null || reportId == null || reportId.isEmpty()) {
            throw new IllegalArgumentException();
        }

        URL url = new URL(HOST + "report/" + reportId + "/voicenote");

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        DataInputStream inStream = null;
        //String existingFileName = file_path;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1*1024*1024;

        //------------------ CLIENT REQUEST
        FileInputStream fileInputStream = new FileInputStream(voiceNote);
        // Open a HTTP connection to the URL
        conn = (HttpURLConnection) url.openConnection();
        // Allow Inputs
        conn.setDoInput(true);
        // Allow Outputs
        conn.setDoOutput(true);
        // Don't use a cached copy.
        conn.setUseCaches(false);
        // Use a post method.
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
        conn.setRequestProperty("API-Key", API_KEY);
        dos = new DataOutputStream( conn.getOutputStream() );
        dos.writeBytes(twoHyphens + boundary + lineEnd);

        dos.writeBytes("Content-Disposition: form-data; name=\"voice_note\";filename=\"" + voiceNote.getName() + "\"" + lineEnd); // uploaded_file_name is the Name of the File to be uploaded
        dos.writeBytes(lineEnd);
        bytesAvailable = fileInputStream.available();
        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];
        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        while (bytesRead > 0){
            dos.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        dos.writeBytes(lineEnd);
        dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
        fileInputStream.close();
        dos.flush();
        dos.close();

        //------------------ read the SERVER RESPONSE
        inStream = new DataInputStream ( conn.getInputStream() );
        String str;
        while (( str = inStream.readLine()) != null){
            Log.d("Debug","Server Response "+str);
        }
        inStream.close();
    }

    public void sendReportPhoto(Report report) {

    }
}
