package com.example.romero.geolocation_report_test.models;

/**
 * Created by romero on 08/07/15.
 */
public enum ReportStatus {
    RECEIVED,
    VALIDATED,
    INVALIDATED,
    RESOLVED
}
