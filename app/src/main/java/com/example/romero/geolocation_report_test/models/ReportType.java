package com.example.romero.geolocation_report_test.models;

/**
 * Created by romero on 08/07/15.
 */
public enum ReportType {
    CRIME,
    MEDICAL_ASSIST,
    WATER_LEAK,
    TRAFFIC_ACCIDENT,
    BROKEN_LIGHT,
    TRAFFIC_LIGHTS,
    TRASH,
    BROKEN_STREET,
    OTHER
}
