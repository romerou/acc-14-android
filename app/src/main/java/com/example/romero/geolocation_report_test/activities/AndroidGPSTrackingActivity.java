package com.example.romero.geolocation_report_test.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.media.MediaRecorder;
import android.util.Log;

import com.example.romero.geolocation_report_test.controllers.GPSTracker;
import com.example.romero.geolocation_report_test.R;
import com.example.romero.geolocation_report_test.models.Report;
import com.example.romero.geolocation_report_test.models.ReportType;
import com.example.romero.geolocation_report_test.tasks.SendNewReportTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AndroidGPSTrackingActivity extends Activity {

    private static final String ROOT_PATH = Environment.getExternalStorageDirectory().getPath() + "/Chacao";
    private static final String VOICE_NOTES_FOLDER_PATH = ROOT_PATH + "/Voice Notes/";
    private static final String PHOTOS_FOLDER_PATH = ROOT_PATH + "/Photos/";
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private MediaRecorder recorder = null;
    private int currentFormat = 0;
    private int output_formats[] = { MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP };
    private String file_exts[] = { AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP };

    Button btnShowLocation;
    Button btnSendLocation;

    // GPSTracker class
    private GPSTracker gps;

    private File photo;
    private File voiceNote;

    private long duration;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        File file = new File(VOICE_NOTES_FOLDER_PATH);
        if(!file.exists()) {
            file.mkdirs();
        }
        file = new File(PHOTOS_FOLDER_PATH);
        if(!file.exists()) {
            file.mkdirs();
        }

        setContentView(R.layout.activity_android_gpstracking);

        btnShowLocation = (Button) findViewById(R.id.btnShowLocation);
        // show location button click event
        btnShowLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
            // create class object
            gps = new GPSTracker(AndroidGPSTrackingActivity.this);

            // check if GPS enabled
            if(gps.canGetLocation()){

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                // \n is for new line
                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            }else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
            }
        });

        btnSendLocation = (Button) findViewById(R.id.btnSendLocation);
        /*
        btnSendLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // create class object
                gps = new GPSTracker(AndroidGPSTrackingActivity.this);

                // check if GPS enabled
                if(gps.canGetLocation()){

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();
                    // TODO
                    String deviceId = "Nexus 7";

                    // Sending location to backend
                    SendNewReportTask reporter = new SendNewReportTask(AndroidGPSTrackingActivity.this);
                    reporter.execute(new Report(deviceId, ReportType.CRIME, longitude, latitude));
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
            }
        });
        */

        btnSendLocation.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                gps = new GPSTracker(AndroidGPSTrackingActivity.this);

                if(gps.canGetLocation()){
                    switch(event.getAction()){
                        case MotionEvent.ACTION_DOWN:
                            duration = System.currentTimeMillis();
                            Log.i("AudioRecorder", "Start Recording");
                            try {
                                startRecording();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            duration = System.currentTimeMillis() - duration;
                            Log.i("AudioRecorder", String.valueOf(event.getDownTime()));
                            Log.i("AudioRecorder", String.valueOf(event.getEventTime()));
                            Log.i("AudioRecorder", String.valueOf(duration));

                            Log.i("AudioRecorder", "stop Recording");
                            stopRecording();

                            double latitude = gps.getLatitude();
                            double longitude = gps.getLongitude();
                            // TODO
                            String deviceId = "Nexus 7";

                            // Sending location to backend
                            Report report = new Report(deviceId, ReportType.CRIME, longitude, latitude);
                            SendNewReportTask reporter = new SendNewReportTask(AndroidGPSTrackingActivity.this);
                            if (voiceNote != null) {
                                if (duration > 1000)
                                    report.setVoiceNote(voiceNote);
                                else
                                    voiceNote.delete();
                            }
                            reporter.execute(report);

                            voiceNote = null;
                            break;
                    }
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }
                return false;
            }
        });
    }

    /*
    private String getFilename(){
        String rootFilePath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,AUDIO_RECORDER_FOLDER);

        if(!file.exists()){
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat]);
    }
    */

    private void startRecording() throws IOException {
        this.recorder = new MediaRecorder();
        this.recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        this.recorder.setOutputFormat(output_formats[currentFormat]);
        this.recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        //this.recorder.setOutputFile(getFilename());
        this.recorder.setOnErrorListener(errorListener);
        this.recorder.setOnInfoListener(infoListener);

        this.voiceNote = new File(VOICE_NOTES_FOLDER_PATH + System.currentTimeMillis() + file_exts[currentFormat]);
        FileOutputStream fos = new FileOutputStream(voiceNote);
        this.recorder.setOutputFile(fos.getFD());

        try {
            this.recorder.prepare();
            this.recorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MediaRecorder.OnErrorListener errorListener = new        MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Log.i("AudioRecorder", "Error: " + what + ", " + extra);
        }
    };

    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            Log.i("AudioRecorder", "Warning: " + what + ", " + extra);
        }
    };

    private void stopRecording(){
        if(this.recorder != null){
            try {
                this.recorder.stop();
            } catch (Exception e) {
                Log.e("AudioRecorder", e.getMessage(), e);
                voiceNote.delete();
            } finally {
                this.recorder.reset();
                this.recorder.release();
                this.recorder = null;
            }
        }
    }

}
