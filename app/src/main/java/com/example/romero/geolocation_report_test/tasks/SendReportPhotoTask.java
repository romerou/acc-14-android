package com.example.romero.geolocation_report_test.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.romero.geolocation_report_test.controllers.BackendConnector;
import com.example.romero.geolocation_report_test.models.Report;

/**
 * Created by romero on 08/07/15.
 */
public class SendReportPhotoTask extends AsyncTask<Report, Void, Boolean> {

    private ProgressDialog dialog;
    private Context appContext;

    public SendReportPhotoTask(Context context) {
        super();
        this.dialog = new ProgressDialog(context);
        this.appContext = context;
    }

    @Override
    protected void onPreExecute() {
        this.dialog.setMessage("Enviando foto...");
        this.dialog.show();
    }

    @Override
    protected Boolean doInBackground(Report... reports) {
        Report report = reports[0];
        BackendConnector conn = new BackendConnector();
        try {
            conn.sendReportVoiceNote(report);
        } catch (Exception e) {
            Log.e("SendReportPhotoTask", e.getMessage(), e);
            return new Boolean(false);
        }
        return new Boolean(true);
    }

    @Override
    protected void onPostExecute(final Boolean result) {
        if (this.dialog.isShowing()) { // if dialog box showing = true
            this.dialog.dismiss(); // dismiss it
        }
        if (result.booleanValue())
            Toast.makeText(appContext, "Foto enviada", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(appContext, "Error en el envío", Toast.LENGTH_LONG).show();
    }

}