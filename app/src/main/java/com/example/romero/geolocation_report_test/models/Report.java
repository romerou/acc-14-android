package com.example.romero.geolocation_report_test.models;

import java.io.File;

/**
 * Created by romero on 03/07/15.
 */
public class Report {

    private String id;
    private String deviceId;
    private ReportType type;
    private double longitude;
    private double latitude;
    private String description;
    private File voiceNote;
    private String voiceNoteURL;
    private File photo;
    private String photoURL;
    private ReportStatus status;

    public Report(String deviceId, ReportType type, double longitude, double latitude) {
        this.deviceId = deviceId;
        this.type = type;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public ReportType getType() {
        return type;
    }

    public void setType(ReportType type) {
        this.type = type;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public File getVoiceNote() {
        return voiceNote;
    }

    public void setVoiceNote(File voiceNote) {
        this.voiceNote = voiceNote;
    }

    public String getVoiceNoteURL() {
        return voiceNoteURL;
    }

    public void setVoiceNoteURL(String voiceNoteURL) {
        this.voiceNoteURL = voiceNoteURL;
    }

    public File getPhoto() {
        return photo;
    }

    public void setPhoto(File photo) {
        this.photo = photo;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }
}
